import java.util.LinkedList;
import java.util.Scanner;

/**
CompletionProcess class finalises transactions.

Manages change and receipts after all products have been processed
*/
public class CompletionProcess {
	
	@SuppressWarnings("resource")
        /**
        Complete transaction method finalises transaction once user says no more items need processing
        @param transactions a list of all the items in the transaction
        @param transactionTotal the accumulative cost of the transaction
        @param savings savings in the transaction if a discount was applied
        @param refundAmount refund amount if a refund is processed
        @param cashOutAmount cash out in transaction
        */
	public static void completeTransaction(LinkedList<Transaction> transactions,
                                               double transactionTotal,
                                               double savings,
                                               double refundAmount,
                                               double cashOutAmount) {
		//Variable that stores the required change for the user
		double changeRequired = 0;
		//Variable that stores the user's cash tendered amount
		double cashTendered = 0;
		//Variable that stores whether the user has entered an expected input or not
		boolean validInput = false;
		
		//Print menu title
		System.out.println("\n              ***             ");
		System.out.println("\n[5] - Complete Transaction");
		System.out.println(" _____________________________");
		
		//Scanner to read in user input
		Scanner reader = new Scanner(System.in);
		
		//If user buys an item (i.e. not just a refund transaction)
		if (transactions.size() > 0) {
			//While the user does not enter an expected input
			while (validInput == false) {
				try {
					//Retrieve cash amount tendered from the user
					System.out.print("\nPlease enter the cash amount tendered: $");
					cashTendered = reader.nextDouble();
					
					//If user input is less than one
					if (cashTendered < 0) {
						//Display an error message
						System.out.println("! - Invalid input.");
						System.out.println("Please enter a value greater than 0.");
					}
					//Otherwise, user has entered an expected input
					else {
						validInput = true;
					}	
				}
				//If an InputMismatchException is caught
				catch (Exception InputMismatchException) {
					//Read the user input
					reader.next();
					
					//Display an error message
					System.out.println("! - Invalid input.");
					System.out.println("Please enter an integer number greater or equal to 1.");
				}
			}
			
			//Calculating changed required
			changeRequired = cashTendered - transactionTotal + cashOutAmount;
			//Printing change required to screen
			System.out.println("\nAmount of change required = $" + changeRequired);
		}
		
		//Printing successful message
		System.out.println("\nTransaction successfully completed!");
		
		//Prompt the user if they would like a receipt
		System.out.print("\n\nWould you like to print a receipt? [y/n]");
		String receiptOption = reader.next();
		
    	//If user input does not equal 'y' or 'n'
		while (!receiptOption.equals("y") && !receiptOption.equals("n")) {
			//Display error message
			System.out.println("\n! - Invalid input.");
			System.out.println("Please select [y]es to print a receipt or [n]o to exit.");
			System.out.print("\nWould you like to print a receipt? [y]es or [n]o: ");
		
			//Retrieve new user input
			receiptOption = reader.next();
		}
		
		//If user would like to print a receipt
		if (receiptOption.equals("y")) {
			System.out.println("\n\n ------- Store Receipt -------\n");
			
			//Headings of receipt columns
			String heading1 = "Item Name";
			String heading2 = "Item Cost";
		
			//Formating titles and columns of the receipt
			System.out.printf("%-15s %15s %n", heading1, heading2);
		
			//For each item that exist in the linked list
			for (int i = 0; i < transactions.size(); i++) {
				//Retrieve each item
				Transaction item = transactions.get(i);
			
				//Retrieve the item name
				String itemName = item.getName();
				//Retrieve the item cost
				double itemCost = item.getCost();
			
				//Print to the screen the item name and cost within its corresponding formatted column
				System.out.printf(String.format("%-15s %15s %n", itemName, "$" + itemCost));
			}
			
			//Print to the screen the refund total
			System.out.println("\n > Refunds: $" + refundAmount);
			//Print to the screen the discount total
			System.out.println(" > Discount: $" + savings);
			//Print to the screen the transaction total
			System.out.println("\nTotal: $" + transactionTotal);
			
			System.out.println("\n              ***             ");
			//Print to the screen the cash tendered
			System.out.println("Cash Tendered: $" + cashTendered);
			//Print to the screen the total change
			System.out.println("Change: $" + changeRequired);
			//Print to the screen the cash out amount
			System.out.println("Cash out: $" + cashOutAmount);
		
			System.out.println("\n -----------------------------\n");
		}
	}
}
