/**
Transaction class manages main functions for individual transactions

Manages names and costs
*/
public class Transaction
{
   private String name;
   private double cost;

   /**
   Transaction constructor
   Initialises name and cost to set values in parameters
   @param name of item
   @param cost of item
   */
   public Transaction(String name, double cost) {
      this.name = name;
      this.cost = cost;
   }

   /**
   Gets cost of specified object
   @return cost of item
   */
   public double getCost() {
      return cost;
   }

   /**
   sets cost of specified object
   @param cost of item
   */
   public void setCost(double cost) {
      this.cost = cost;
   }

   /**
   Gets name of specified object
   @return name of item
   */
   public String getName() {
      return name;
   }

   /**
   sets name of specified object
   @param name of item
   */
   public void setName(String name) {
      this.name = name;
   }

}
