import java.util.Scanner;

/**
Discount process manages the discounting 

Allows for a concession discount
*/
public class DiscountProcess {
	
	@SuppressWarnings("resource")
        /**
        discount method that applies a discount to a transaction total when applicable
        @param transactionTotal the total of the transaction before any discount
        @param savings current savings
        @return savings caused by discount on transaction
        */
	public static double discount(double transactionTotal, double savings) {
		//Variable that stores whether the user has more discounts to enter or not
		boolean moreItems = true;
		
		//Print menu title
		System.out.println("\n              ***             ");
		System.out.println("\n[3] - Enter a discount");
		System.out.println(" _____________________________");
		
		//Scanner to read in user input
		Scanner reader = new Scanner(System.in);
		
		//While the user still has more discounts to enter
		while (moreItems == true) {
			//Retrieve the discount code from the user
			System.out.print("\nPlease enter your discount code: ");
			String discountCode = reader.next();
			
			//A list of valid discounts: 'STUDENT' or 'SENIOR'
			String[] validDiscounts = {"STUDENT", "SENIOR"};
			
			//Check if the discount code was entered correctly by the user
			boolean valid = false;
			for (int i = 0; i < validDiscounts.length; i++) {
				if (validDiscounts[i].equalsIgnoreCase(discountCode)) {
					valid = true;
				}
			}
			
			//If the discount code entered was not valid
			if (!valid) {
				//Print an invalid message
				System.out.println("! - Invalid code");
			}
			//If the discount code entered was valid
			else {
				//Apply a discount of 15% to the total cost of the transaction
				savings = transactionTotal * 0.15;
				//Print a valid message
				System.out.println("Code Applied. You saved 15%!, thats a $" + savings + " saving.");
			}
			
			//Prompt the user if they would like to enter another discount
	    	System.out.print("\n\nWould you like to enter another discount? [y]es or [n]o: ");
	    	String anotherItem = reader.next();
		
	    	//If user input does not equal 'y' or 'n'
			while (!anotherItem.equals("y") && !anotherItem.equals("n")) {
				//Display error message
				System.out.println("! - Invalid input.");
				System.out.println("Please select [y]es to enter another discount or [n]o to finish transaction.");
				System.out.print("\n\nWould you like to enter another discount? [y]es or [n]o: ");
			
				//Retrieve new user input
				anotherItem = reader.next();
			}
			
			//If the user input equals "n"
			if (anotherItem.equals("n")) {
				//No more discounts to be entered
				moreItems = false;
			}
		}
		
		//Return the discount amount
		return savings;
	}
}