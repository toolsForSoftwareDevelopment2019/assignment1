import java.util.Scanner;

public class CashOutProcess {
	
	@SuppressWarnings("resource")
	public static double cashOut() {
		//Variable that stores the cash out amount
		double cashOutAmount = 0;
		//Variable that stores whether the user has entered an expected input or not
		boolean validInput = false;
				
		//Print menu title
		System.out.println("\n              ***             ");
		System.out.println("\n[4] - Request cash out");
		System.out.println(" _____________________________");
		
		//Scanner to read in user input
		Scanner reader = new Scanner(System.in);
				
		//While the user does not enter an expected input
		while (validInput == false) {
			try {
				//Retrieve cash out amount from the user
				System.out.print("\nPlease enter the amount of cash you would like to withdraw: $");
				cashOutAmount = reader.nextDouble();
				
				//If user input is less than one
				if (cashOutAmount < 1) {
					//Display an error message
					System.out.println("! - Invalid input.");
					System.out.println("Please enter a value greater than 0.");
				}
				//Otherwise, user has entered an expected input
				else {
					validInput = true;
				}	
			}
			//If an InputMismatchException is caught
			catch (Exception InputMismatchException) {
				//Read the user input
				reader.next();
				
				//Display an error message
				System.out.println("! - Invalid input.");
				System.out.println("Please enter an integer number greater or equal to 1.");
			}
		}
		
		//Print success message to the screen
		System.out.println("\n > You have added $" + cashOutAmount + " cash out to your transaction");
		
		//Return cash out amount
		return cashOutAmount;
	}
	
}