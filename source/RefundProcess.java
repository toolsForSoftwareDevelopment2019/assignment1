import java.util.Scanner;

/**
Refund process manages refunds in transactions
*/
public class RefundProcess {

        /**
        refund method that allows products to be refunded in the case of being faulty
        @return refund amount on transaction
        */
	@SuppressWarnings("resource")
	public static double refund() {
		//Variable that stores the cost of an item
		double refundAmount = 0;
		//Variable that stores the quantity of an item
		double itemQuantity = 0;
		//Variable that stores whether the user has entered an expected input or not
		boolean validInput = false;
		//Variable that stores whether the user has more items to refund or not
		boolean moreItems = true;
		
		//Print menu title
		System.out.println("\n              ***             ");
		System.out.println("\n[2] - Request a refund");
		
		//Scanner to read in user input
		Scanner reader = new Scanner(System.in);	
		
		//While the user still has items to refund
		while (moreItems == true) {
			System.out.println(" _____________________________");
			
			//Retrieve the name of the item from the user
			System.out.print("\n > Please enter the name of the item to be refunded: ");
	        String itemName = reader.next();
	        
	        //While a name is not entered or is null
	        while (itemName.equals(null)) {
	    		//Display an error message
	    		System.out.println(" ! - Please enter a valid name.");
	    		System.out.print(" \n> Please enter the item's name: ");
	    	
	    		//Retrieve new item name
	        	itemName = reader.nextLine();
	    	}
	        
	        //Reassign false to the variable validInput to act as a loop control variable
			validInput = false;
	        //While the user does not enter an expected input
			while (validInput == false) {
				try {
					//Retrieve the item's cost from the user
					System.out.print("\n > Please enter the item's cost: $");
					refundAmount = reader.nextDouble();
					
					//If user input is less than zero
					if (refundAmount < 0) {
						//Display an error message
						System.out.println("! - Invalid input.");
						System.out.println("Please enter a value greater or equal to $0.");
					}
					//Otherwise, the user has entered an expected input
					else {
						validInput = true;
					}	
				}
				//If an InputMismatchException is caught
				catch (Exception InputMismatchException) {
					//Read the user input
					reader.next();
					
					//Display an error message
					System.out.println("! - Invalid input.");
					System.out.println("Please enter a valid cost price.");
				}
			}
			
			//Reassign false to the variable validInput to act as a loop control variable
			validInput = false;
			//While the user does not enter an expected input
			while (validInput == false) {
				try {
					//Retrieve the quantity of the item from the user
					System.out.print("\n > Please enter the quantity of the item: ");
					itemQuantity = reader.nextInt();
					
					//If user input is less than one
					if (itemQuantity < 1) {
						//Display an error message
						System.out.println("! - Invalid input.");
						System.out.println("Please enter a value greater than 0.");
					}
					//Otherwise, user has entered an expected input
					else {
						validInput = true;
					}	
				}
				//If an InputMismatchException is caught
				catch (Exception InputMismatchException) {
					//Read the user input
					reader.next();
					
					//Display an error message
					System.out.println("! - Invalid input.");
					System.out.println("Please enter an integer number greater or equal to 1.");
				}
			}
			
			//Calculate the total refund amount of the item(s) by multiplying the cost with the quantity of the item
			refundAmount = refundAmount * itemQuantity;
			
			//Print a success message to the screen
			System.out.println("\nItem has been successfully refunded.");
			
			//Prompt the user if they would like to refund another item
	    	System.out.print("\n\nWould you like to refund another item? [y]es or [n]o: ");
	    	String anotherItem = reader.next();
		
	    	//If user input does not equal 'y' or 'n'
			while (!anotherItem.equals("y") && !anotherItem.equals("n")) {
				//Display error message
				System.out.println("\n! - Invalid input.");
				System.out.println("Please select [y]es to refund another item or [n]o to finish transaction.");
				System.out.print("\nWould you like to refund another item? [y]es or [n]o: ");
			
				//Retrieve new user input
				anotherItem = reader.next();
			}
			
			//If the user input equals "n"
			if (anotherItem.equals("n")) {
				//No more items are to be refunded
				moreItems = false;
			}
		}
		
		//Return the refund amount
		return refundAmount;
	}
}