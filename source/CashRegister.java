import java.util.LinkedList;
import java.util.Scanner;

/**
Cash Register Class is the main manager of the registers

Manages transactions and all features
*/
public class CashRegister {

	@SuppressWarnings("resource")

    /**
    * Main method that collates all functions and classes to complete the cash registers intended use
    * @param args
    * */
	public static void main(String[] args) {
		//Variable that stores the user's menu choice
		int menuChoice = 0;
		//Variable that stores the discount amount
		double discounts = 0;
		//Variable that stores the float value of the cash register
		double floatValue = 0;
		//Variable that stores the total cost of all the items in the transaction
		double totalCost = 0;
		//Variable that stores the refund amount of all items refunded by the user
		double refundCost = 0;
		//Variable that stores the amount of money user requested for cash out
		double cashOutCount = 0;
		//Boolean variable that stores whether the user has entered an expected input or not
		boolean validInput = false;
		
		//Linked list to store all items within a transaction
		LinkedList<Transaction> transaction = new LinkedList<Transaction>();

		//Welcome message
		System.out.println("\n *****************************");
		System.out.println(" *                           *");
		System.out.println(" *   Welcome to the Store!   *");
		System.out.println(" *                           *");
		System.out.println(" *****************************");
		
		//While the user does not enter an expected input
		while (validInput == false) {
			//Scanner to read in user input
			Scanner reader = new Scanner(System.in);
			
			try {
				//Retrieve cash register's float value from the user
				System.out.print("\nPlease enter the cash register's float: $");
				floatValue = reader.nextDouble();
				
				//If user input is less than one
				if (floatValue < 1) {
					//Display an error message
					System.out.println("! - Invalid input.");
					System.out.println("Please enter a value greater than $0.");
				}
				//Otherwise, user has entered an expected input
				else {
					validInput = true;
				}	
			}
			//If an InputMismatchException is caught
			catch (Exception InputMismatchException) {
				//Read the user input
				reader.next();
				
				//Display an error message
				System.out.println("! - Invalid input.");
				System.out.println("Please enter a valid numerical value.");
			}
		}
		
		//While the menu choice is 0
		while (menuChoice == 0) {
			//Call the menu method in the class ProcessMenu
			menuChoice = ProcessMenu.menu(menuChoice);
			
			//If the user chooses the menu feature [0. View register balance]
			if (menuChoice == 0) {
				//Print menu title 
				System.out.println("\n              ***             ");
				System.out.println("\n[0] - View register balance");
				System.out.println(" _____________________________");
				
				//Update the cash registers float
				floatValue = floatValue - totalCost;
				//Display the cash register float amount to the screen
				System.out.println("\nBalance of the Cash Register: $" + Double.toString(floatValue));
			
				//Continue the while loop to display the menu again
				menuChoice = 0;
			}
			//If the user chooses the menu feature [1. Enter an Item]
			else if (menuChoice == 1) {
				//Call the process method in the class TransactionProcess
				transaction = TransactionProcess.process(transaction);
				
				//For each item in the linked list
				for (int i = 0; i < transaction.size(); i++) {
					//Retrieve each item in the transaction
					Transaction item = transaction.get(i);
					//Get the total cost of each individual item and add it together to find the total cost of the transaction
					totalCost = totalCost + item.getCost();
				}
				
				//Continue the while loop to display the menu again
				menuChoice = 0;
			}
			//If the user chooses the menu feature [2. Request a refund]
			else if (menuChoice == 2) {
				//Call the refund method in the class RefundProcess
				refundCost = RefundProcess.refund();
				
				//Subtract the refund amount from the total cost of the transaction
				totalCost = totalCost - refundCost;
				//Subtract the refund amount from the float value
				floatValue = floatValue - refundCost;
				
				//Continue the while loop to display the menu again
				menuChoice = 0;
			}
			//If the user chooses the menu feature [3. Enter a discount]
			else if (menuChoice == 3) {
				//Call the discount method in the class DiscountProcess
				discounts = DiscountProcess.discount(totalCost, discounts);
				
				//Subtract the discount from the total cost of all items
				totalCost = totalCost - discounts;
				//Subtract the discount from the float value
				floatValue = floatValue - discounts;
				
				//Continue the while loop to display the menu again
				menuChoice = 0;
			}
			//If the user chooses the menu feature [4. Request cash out]
			else if (menuChoice == 4) {
				//Call the cashOut method in the class CashOutProcess
				cashOutCount = CashOutProcess.cashOut();
				
				//Subtract cash out amount from float value
				floatValue = floatValue - cashOutCount;
				
				//Continue the while loop to display the menu again
				menuChoice = 0;
			}
			//If the user chooses the menu feature [5. Complete transaction]
			else if (menuChoice == 5) {
			    //Call the completeTransaction method in the class CompletionProcess
				CompletionProcess.completeTransaction(transaction, totalCost, discounts, refundCost ,cashOutCount);
				
				//Show the current balance of the cash register
				System.out.println("Current balance of the cash register: $" + floatValue);
				
				//Prompt the user to exit the store or make another transaction
				System.out.println("\nWould you like to make another transaction? [y/n]");
				Scanner reader = new Scanner(System.in);
				String option = reader.next();
				
				//If the user input does not equal 'y' or 'n'
				while (!option.equals("y") && !option.equals("n")) {
					//Display error message
					System.out.println("\n! - Invalid input.");
					System.out.println("Please select [y]es to make another transaction or [n]o to exit.");
					System.out.print("\nWould you like to make another transaction? [y/n]");
				
					//Retrieve new user input
					option = reader.next();
				}
				
				if (option.equals("y")) {
					menuChoice = 0;
				}
				else {
					menuChoice = 6;
					//Exit message
					System.out.println("\n\n *****************************************");
					System.out.println(" *                                       *");
					System.out.println(" *   Thank you for entering the Store!   *");
					System.out.println(" *                                       *");
					System.out.println(" *****************************************");
				}
			}
			//If the user chooses the menu feature [6. Exit]
			else if (menuChoice == 6) {
				//Exit message
				System.out.println("\n\n *****************************************");
				System.out.println(" *                                       *");
				System.out.println(" *   Thank you for entering the Store!   *");
				System.out.println(" *                                       *");
				System.out.println(" *****************************************");
			}
		}
	}
}