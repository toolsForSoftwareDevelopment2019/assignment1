import java.util.Scanner;

/**
Process menu class manages the register menu
*/
public class ProcessMenu {

	@SuppressWarnings("resource")
        /**
        menu method that manages a user-friendly interface on the console
        @param numberChoice the users menu choice chosen
        @return number chosen of menu options
        */
	public static int menu(int numberChoice) {
		//Variable that stores whether the user has entered an expected input or not
		boolean validInput = false;
		
		//Print menu options
		System.out.println("\n _____________________________");
		System.out.println(" ----------- Menu ------------");
		System.out.println("\n 0. View register balance");
		System.out.println(" -----------------------------");
		System.out.println(" 1. Enter an item");
		System.out.println(" 2. Request a refund");
		System.out.println(" 3. Enter a discount");
		System.out.println(" 4. Request cash out");
		System.out.println(" 5. Complete transaction");
		System.out.println(" -----------------------------");
		System.out.println(" 6. Exit");
		System.out.println(" _____________________________");
		
		//Scanner to read in user input
		Scanner reader = new Scanner(System.in);
		
		//While the user does not enter an expected input
		while (validInput == false) {
			try {
				//Retrieve the user's chosen menu feature
				System.out.print("\nPlease enter a menu item [0, 1, 2, 3, 4, 5, 6]: ");
				numberChoice = reader.nextInt();
				
				//If the user's entered number is not on the menu
				if (numberChoice < 0 || numberChoice >= 7) {
					//Display an error message
					System.out.println("! - Invalid input.");
					System.out.println("Please enter a number from 0 to 6.");
				}
				//Otherwise, the user has entered an expected input
				else {
					validInput = true;
				}	
			}
			//If an InputMismatchException is caught
			catch (Exception InputMismatchException) {
				//Read the user input
				reader.next();
				
				//Display an error message
				System.out.println("! - Invalid input.");
				System.out.println("Please enter a valid numerical value from 0 to 6.");
			}
		}
		
		//Return the user's chosen number
		return numberChoice;
	}
}