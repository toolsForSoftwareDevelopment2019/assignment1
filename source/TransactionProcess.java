import java.util.LinkedList;
import java.util.Scanner;

/**
Transaction process class to manages items in transactions
*/
public class TransactionProcess {
	
	@SuppressWarnings("resource")
        /**
        process method that manages all items in a transaction
        @param transactions a list of items in transaction
        @return items in this transaction
        */
	public static LinkedList<Transaction> process(LinkedList<Transaction> transactions) {		
		//Variable that stores the name of an item
		String itemName = null;
		//Variable that stores the cost of an item
		double itemCost = 0;
		//Variable that stores the quantity of an item
		int itemQuantity = 0;
		//Variable that stores whether the user has entered an expected input or not
		boolean validInput = false;
		//Variable that stores whether the user has more items to refund or not
		boolean moreItems = true;
		
		//Print menu title
		System.out.println("\n              ***             ");
		System.out.println("\n[1] - Enter a transaction");
		
		//Scanner to read in user input
		Scanner reader = new Scanner(System.in);	
		
		//While the user still has items to be entered
		while (moreItems == true) {
			System.out.println(" _____________________________");
			
			//Retrieve the name of the item from the user
			System.out.print("\n > Please enter the item's name: ");
			itemName = reader.next();
		
			//While a name is not entered or is null
			while (itemName.equals(null)) {
        		//Display an error message
        		System.out.println(" ! - Please enter a valid name.");
        		System.out.print(" \n> Please enter the item's name: ");
        	
        		//Retrieve new item name
	        	itemName = reader.nextLine();
        	}
			
			//Reassign false to the variable validInput to act as a loop control variable
			validInput = false;
			//While the user does not enter an expected input
			while (validInput == false) {
				try {
					//Retrieve the item's cost from the user
					System.out.print("\n > Please enter the item's cost: $");
					itemCost = reader.nextDouble();
					
					//If user input is less than zero
					if (itemCost < 0) {
						//Display an error message
						System.out.println("! - Invalid input.");
						System.out.println("Please enter a value greater or equal to $0.");
					}
					//Otherwise, the user has entered an expected input
					else {
						validInput = true;
					}	
				}
				//If an InputMismatchException is caught
				catch (Exception InputMismatchException) {
					//Read the user input
					reader.next();
					
					//Display an error message
					System.out.println("! - Invalid input.");
					System.out.println("Please enter a valid cost price.");
				}
			}
			
			//Reassign false to the variable validInput to act as a loop control variable
			validInput = false;
			//While the user does not enter an expected input
			while (validInput == false) {
				try {
					//Retrieve the quantity of the item from the user
					System.out.print("\n > Please enter the quantity of the item: ");
					itemQuantity = reader.nextInt();
					
					//If user input is less than one
					if (itemQuantity < 1) {
						//Display an error message
						System.out.println("! - Invalid input.");
						System.out.println("Please enter a value greater than 0.");
					}
					//Otherwise, user has entered an expected input
					else {
						validInput = true;
					}	
				}
				//If an InputMismatchException is caught
				catch (Exception InputMismatchException) {
					//Read the user input
					reader.next();
					
					//Display an error message
					System.out.println("! - Invalid input.");
					System.out.println("Please enter an integer number greater or equal to 1.");
				}
			}
			
			//Calculate the total amount of the item by multiplying the cost with the quantity
			itemCost = itemCost * itemQuantity;
			
			//Create a new transaction item
			Transaction trans = new Transaction(itemName, itemCost);
			//Add the transaction to the linked list
			transactions.add(trans);
			
			//Print a success message to the screen
			System.out.println("\nItem has been successfully processed.");
			
			//Prompt the user if they would like to add another item
	    	System.out.print("\n\nWould you like to add another item? [y]es or [n]o: ");
	    	String anotherItem = reader.next();
		
	    	//If user input does not equal 'y' or 'n'
			while (!anotherItem.equals("y") && !anotherItem.equals("n")) {
				//Display error message
				System.out.println("\n! - Invalid input.");
				System.out.println("Please select [y]es to add another item or [n]o to finish transaction: ");
				System.out.print("\nWould you like to add another item? [y]es or [n]o: ");
			
				//Retrieve new user input
				anotherItem = reader.next();
			}
			
			//If the user input equals "n"
			if (anotherItem.equals("n")) {
				//No more items are to be refunded
				moreItems = false;
			}
		}
		
		//Return the transactions linked list
		return transactions;
	}
}